# sevendays

This is my UI slicing project from Figma to Flutter with Dart.

This project have 8 page which is :
1. Empty State
2. Get Started 
3. Home
4. Pricing Screen
5. Random Screen
6. Rating Screen
7. Sign In 
8. Splash Screen. 

I use GetX framework for this project and that has different feature on each pages.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

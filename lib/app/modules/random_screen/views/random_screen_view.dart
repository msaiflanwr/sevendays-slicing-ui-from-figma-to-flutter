import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/random_screen_controller.dart';

class RandomScreenView extends GetView<RandomScreenController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('RandomScreenView'),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          'RandomScreenView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}

import 'package:get/get.dart';

import '../controllers/random_screen_controller.dart';

class RandomScreenBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RandomScreenController>(
      () => RandomScreenController(),
    );
  }
}

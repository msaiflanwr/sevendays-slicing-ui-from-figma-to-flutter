import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/pricing_screen_controller.dart';

class PricingScreenView extends GetView<PricingScreenController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PricingScreenView'),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          'PricingScreenView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}

import 'package:get/get.dart';

class PricingScreenController extends GetxController {
  //TODO: Implement PricingScreenController

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
}

import 'package:get/get.dart';

import '../controllers/pricing_screen_controller.dart';

class PricingScreenBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PricingScreenController>(
      () => PricingScreenController(),
    );
  }
}

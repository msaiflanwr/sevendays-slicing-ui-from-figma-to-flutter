import 'package:get/get.dart';

import '../controllers/empty_state_controller.dart';

class EmptyStateBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EmptyStateController>(
      () => EmptyStateController(),
    );
  }
}

import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/empty_state_controller.dart';

class EmptyStateView extends GetView<EmptyStateController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('EmptyStateView'),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          'EmptyStateView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}

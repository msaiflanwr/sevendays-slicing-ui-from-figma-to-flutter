import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/rating_screen_controller.dart';

class RatingScreenView extends GetView<RatingScreenController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('RatingScreenView'),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          'RatingScreenView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}

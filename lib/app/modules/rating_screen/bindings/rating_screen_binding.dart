import 'package:get/get.dart';

import '../controllers/rating_screen_controller.dart';

class RatingScreenBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RatingScreenController>(
      () => RatingScreenController(),
    );
  }
}

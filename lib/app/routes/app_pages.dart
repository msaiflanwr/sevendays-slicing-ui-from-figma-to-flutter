import 'package:get/get.dart';

import '../modules/empty_state/bindings/empty_state_binding.dart';
import '../modules/empty_state/views/empty_state_view.dart';
import '../modules/get_started/bindings/get_started_binding.dart';
import '../modules/get_started/views/get_started_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/pricing_screen/bindings/pricing_screen_binding.dart';
import '../modules/pricing_screen/views/pricing_screen_view.dart';
import '../modules/random_screen/bindings/random_screen_binding.dart';
import '../modules/random_screen/views/random_screen_view.dart';
import '../modules/rating_screen/bindings/rating_screen_binding.dart';
import '../modules/rating_screen/views/rating_screen_view.dart';
import '../modules/sign_in/bindings/sign_in_binding.dart';
import '../modules/sign_in/views/sign_in_view.dart';
import '../modules/splash_screen/bindings/splash_screen_binding.dart';
import '../modules/splash_screen/views/splash_screen_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.SPLASH_SCREEN,
      page: () => SplashScreenView(),
      binding: SplashScreenBinding(),
    ),
    GetPage(
      name: _Paths.GET_STARTED,
      page: () => GetStartedView(),
      binding: GetStartedBinding(),
    ),
    GetPage(
      name: _Paths.SIGN_IN,
      page: () => SignInView(),
      binding: SignInBinding(),
    ),
    GetPage(
      name: _Paths.EMPTY_STATE,
      page: () => EmptyStateView(),
      binding: EmptyStateBinding(),
    ),
    GetPage(
      name: _Paths.RATING_SCREEN,
      page: () => RatingScreenView(),
      binding: RatingScreenBinding(),
    ),
    GetPage(
      name: _Paths.PRICING_SCREEN,
      page: () => PricingScreenView(),
      binding: PricingScreenBinding(),
    ),
    GetPage(
      name: _Paths.RANDOM_SCREEN,
      page: () => RandomScreenView(),
      binding: RandomScreenBinding(),
    ),
  ];
}

part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const SPLASH_SCREEN = _Paths.SPLASH_SCREEN;
  static const GET_STARTED = _Paths.GET_STARTED;
  static const SIGN_IN = _Paths.SIGN_IN;
  static const EMPTY_STATE = _Paths.EMPTY_STATE;
  static const RATING_SCREEN = _Paths.RATING_SCREEN;
  static const PRICING_SCREEN = _Paths.PRICING_SCREEN;
  static const RANDOM_SCREEN = _Paths.RANDOM_SCREEN;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const SPLASH_SCREEN = '/splash-screen';
  static const GET_STARTED = '/get-started';
  static const SIGN_IN = '/sign-in';
  static const EMPTY_STATE = '/empty-state';
  static const RATING_SCREEN = '/rating-screen';
  static const PRICING_SCREEN = '/pricing-screen';
  static const RANDOM_SCREEN = '/random-screen';
}
